module Tests (main) where

import Test.HUnit

test1 = TestCase (assertEqual "one is one" 1 1)

tests = TestList [ TestLabel "test1" test1]

main = runTestTT tests